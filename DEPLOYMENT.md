# Deployment of virtualization technologies

Deploying some technologies sometimes requires extra steps.
In this section we describe those steps.

## Installing hypervisor

- Xen requires requires changing the network configuration. Add the correct information in `ansible/group_vars/xen_host`.

Each deployment starts with running ansible for the hypervisor with `ansible-playbook -i inventory.ini phase1.yml -l <technology>_host`

- At this point, bare-metal, Docker and Podman are ready for benchmarking. Use the ip address of the host.
- Xen is prepared for guest setup. Use root user and password set in `ansible/group_vars/xen_host` to access the vm.

### Virtualbox and VMWare Workstation

1. Download and transfer the OS image for the VM to the host server.
1. Open the GUI app remotely if needed:
    1. connect to the server with `ssh -X`
    1. Open the app
1. Create a VM (Refer to the manual if needed)
    - [VirtualBox](https://www.virtualbox.org/manual/ch01.html)
    - [VMWare](https://www.vmware.com/support/pubs/)

### KVM

1. Run the following command: `virt-install --virt-type kvm --name kvm-vm --vcpus 8 --cpu host --location <os-location> --os-variant debian10 --disk size=60,sparse=false --memory 14336 --graphics none --console pty,target_type=serial --extra-args "console=ttyS0" --network bridge=br0 --hvm
`
    1. os-location can be local file or url like [debian distribution](http://deb.debian.org/debian/dists/buster/main/installer-amd64/).
1. Setup the guest OS (Regular OS installation)

## Guest setup

1. Add IP address to the technology guest in the `ansible/inventory.ini`
1. Deploy the benchmark using `ansible-playbook -i inventory.ini phase1.yml -l <technology>_vm` or `baremetal_host`
1. Verify the VTmark API is running by a get request: `http://<guest-ip>:<port>/`

## Monitoring

Once the hypervisor and vm/container are deployed, you will see the resource usage in the Grafana dashboard.

## Possible issues

If the benchmark is not responding on the default port or one of the servers is not showing in the dashboard, try to chek if all services are running.
