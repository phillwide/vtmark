# VTmark Benchmark Executor 

This file describes the use of VTmark executor, a script for benchmarking virtualization technologies remotely.
The results are writen in a set folder structure.

## Folder Structure 

In the results folder, there are folders for each benchmark where the results are saved.

```
results/
├── dhrystone
|   ├── baremetal-1
|   ├── docker-1
|   └── ...
├── whetstone 
└── ...     
```

## Usage 

The script is located under `results/vtmark_executor.py`

### Positional Arguments 

#### Host 

Specify the ip address of the guest, where the benchmarking ansible role was deployed to.

#### Name

Specify the name of the technology benchmark. Used for the filenames of the results.

### Optional Arguments 

#### Skip (-s) 

By default all benchmarks are run. To limit them, use this option and specify the numbers of benchmark to skip.

#### Port (-p) 

Specify which port is the benchmarking API exposed on. By default, its port 4000.

#### Target (-t) 

Specify s server running server part of a benchmark. Relevant for network benchmarking.

```
usage: vtmark_executor.py [-h] [-s SKIP [SKIP ...]] [-p PORT] [-t TARGET] host name

VTMark executor
     Run and save benchmark results.
    You can run the following benchmarks:
	 0: Dhrystone benchmark
	 1: Whetstone benchmark
	 2: Ramspeed benchmark
	 3: Dbench benchmark
	 4: Kernel compilation benchmark
	 5: IOZone
	 6: Bonnie++ benchmark
	 7: iPerf3 benchmark (ip addr)

positional arguments:
  host                  Specify host to benchmark
  name                  Specify name of the host technology

optional arguments:
  -h, --help            show this help message and exit
  -s SKIP [SKIP ...], --skip SKIP [SKIP ...]
                        Specify benchmarks to skip
  -p PORT, --port PORT  Specify host port
  -t TARGET, --target TARGET
                        Specify target server for benchmark
```
