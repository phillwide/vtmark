# VTmark Result Parser 

This file describes the use of VTmark parser, a script for processing the benchmarking results.
The script works with the same folder structure as the executor.

## Adding New Results 

To use the parser process additional results, list the names used with executor and a name for the output in `results/vtmark/technologies.py`.

## Usage

The script is located under `results/vtmark_parser.py`

### Positional Arguments

#### Benchmark

Specify the benchmark to parse by the number assigned to the benchmark in the help output.

### Optional Arguments

#### Excel (-x)

Output the data in an excel (comma separated) format instead of latex table.

#### Relative (-r)

Output the data relative to the first technology (in `results/vtmark/technologies.py`).

#### Threshold (-t)

If the relative standard deviation for any of the technologies is higher than this threshold, the output will also include min and max values.

#### RSTD (-s)

Output relative standard deviation
1. Show rstd per subbenchmark
1. Show maximal rstd per technology
1. Show maximal rstd per benchmark 

```
vtmark_parser.py [-h] [-x] [-r] [-t TRESHOLD] [-s RSTD] benchmark
Choose to run one of the following benchmarks:
    0: All
    1: Dhrystone benchmark
    2: Whetstone benchmark
    3: Ramspeed benchmark
    4: Dbench benchmark
    5: Kernel compilation benchmark
    6: Bonnie++ benchmark
    7: iPerf3 benchmark (ip addr)
positional arguments:
    benchmark                            Select benchmark to process 
optional arguments:
    -h, --help                           Show this help message and exit
    -x, --excel                          Output data in excel format
    -r, --relative                       Output data relative to first item
    -t TRESHOLD, --t reshold TRESHOLD    Change the default relative standard deviation from 5
    -s RSTD, --rstd RSTD                 Output relative standard deviation 1: Show rstd per subbenchmark 2: Show maximal rstd per technology 3: Show maximal rstd per benchmark 
```

## Additional Scripts

### Bonnie++

For output of `vtmark_parser.py 6 -x -r -t 15` there are scripts to remove the CPU:

[Bonnie++ results without CPU](results/vtmark-additional-scripts/bonnie-no-cpu.py)

[Bonnie++ results without CPU all on one line](results/vtmark-additional-scripts/bonnie-no-cpu-together.py)

### Ramspeed

For output of `vtmark_parser.py 3 -x -r` there is a script to add average of the individual benchmarks and overall average.

[Ramspeed average](results/vtmark-additional-scripts/ramspeed-average.py)

## Output columns

The parser does not output the column labels for all benchmarks.
Here we list the column labels for the benchmarks.

### Dhrystone

No optimization, Register, Optimized (-O)

### Whetstone

No Optimization, Optimized

### Ramspeed

The parser outputs the results of several subbenchmarks in the following order:
INTEGER, FL-POINT, MMX, SSE, MMX (nt),  SSE (nt)

Each of these subbenchmarks have the following columns:
Copy, Scale, Add, Triad, Average

### Dbench

The results contain three sections:

1. No synchronization
1. Fsync
1. O_SYNC

Each section contains results for 1, 2, 4 and 8 processes in order.
The results for each number of processes includes average values and latency in MB/sec in order.

### Kernel Compilation

Score, Time (sec), CPU utilization

### Bonnie++

1. Sequential output: Per Char (K/sec, CPU), Block (K/sec, CPU), Rewrite (K/sec, CPU)
1. Sequential input: Per Char (K/sec, CPU), Block (K/sec, CPU)
1. Sequential create: Create (/sec, CPU), Read (/sec, CPU), Delete (/sec, CPU)

### iPerf3

First output is for UDP: Limited server and client, Unlimited server and client

Second output is for TCP: Limited, Unlimited
