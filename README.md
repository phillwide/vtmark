# VTmark

VTmark is a semi-automatic virtualization technologies benchmarking suite.
It is used to setup monitoring and deploy a set of synthetic benchmarks.
It is then used to benchmark the server and capture the results.
Results can be then processed using by supplied scripts.

## Requirements

 - Ansible 2.8 and newer
    - Basic knowledge of using Ansible to be able to deploy to your servers
 - Server for monitoring (At least 2 cores and 2GB of ram, gigabit network adapter)
 - Server for benchmarking
 - Local network with a pool of available ip addresses (For the VM's)
 - For monitoring Grafana 6.5.2 needs to be used for the dashboard to work

## How to use

After preparing servers required, follow the following steps:

### Benchmarking

1. Add IP address of the server for monitoring to `telegraf_influxdb_instance` in `ansible/group_vars/all/vars.yml` and to `monitor` host in `ansible/inventory.ini`
1. In `ansible/group_vars/all/vars.yml` set correct timezone information
1. Change `ansible/ansible.cfg` to match your setup
1. Deploy monitoring with `ansible-playbook -i inventory.ini monitor.yml -l monitor`
    - Check grafana is setup correctly (username: admin, password: 123456), it should contain single dashboard
1. Deploy one of the technologies.
    1. Add IP address to the technology host in the `ansible/inventory.ini`
    1. Deploy the technology, technology specific deployment is described here: [Technology deployment](DEPLOYMENT.md)
1. Run the benchmark using [VTmark executor](EXECUTOR.md)
    1. For iperf3 run `iperf3 -s -d` at the monitoring server beforehand

### Monitoring

The deployment of Grafana includes a dashboard visualizing the resources of the hypervisors and virtual machines.
Adjust the shown time to the time whem you benchmarked to see the effect of the benchmarks on the resources.
The result files contain a timestamp to make it simple.

### Data analysis

The results are saved under `results/<benchmark>/<technology>` or `results/<benchmark>/<technology>-<number>`.

The VTmark suite contains a utility to process the results and output the results for all benchmarked technologies as a table.
It also allows to check the relative standard deviation of the results and relativize the results to one of the technologies.
It can process most of the data collected by the suite. Learn how to use the [parser here](PARSER.md).

## Thesis report

[Thesis report](Thesis Report Filip Siroky.pdf)
