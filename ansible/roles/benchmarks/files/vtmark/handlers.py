#!/usr/bin/python3
import os
import subprocess


location = os.path.dirname(os.path.realpath(__file__))

def iterate(commands, cwd=None):
    output = ""
    for item in commands:
        ret = subprocess.run(item[1], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=cwd)
        output += "{} Output:\n{}".format(item[0], ret.stdout.decode())
        if ret.returncode != 0:
            output += "Returned error: {}\n".format(ret.stderr.decode())
            return output, ret.returncode
    return output, ret.returncode


def disk_benchmark_prepare():
    prepare = ['sync; echo 3 > /proc/sys/vm/drop_caches',
                'fstrim /']
    output = ""
    for item in prepare:
        ret = subprocess.run(item, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if ret.returncode != 0:
            output += "Disk cleanup returned error: {}\n".format(ret.stderr.decode())
    return output, ret.returncode

def dhrystone_build():
    commands = [("", ["/bin/sh", location + "/dhrystone/dhrystone-2.2.c"])]
    return iterate(commands)


def dhrystone_run():
    commands = [("Dhrystone default:", [location + "/dry2", "2147483627"]),
                ("Dhrystone register:", [location + "/dry2nr", "2147483627"]),
                ("Dhrystone optimization:", [location + "/dry2o", "2147483627"])]
    return iterate(commands)


def whetstone_build():
    commands = [("", ["gcc", location + "/whetstone/whetstone.c", "-lm", "-DDP", "-DUNIX", "-o", "whet"]),
                ("", ["gcc", location + "/whetstone/whetstone.c", "-DDP", "-lm", "-O2", "-fomit-frame-pointer",
                      "-ffast-math", "-fforce-addr", "-DUNIX", "-o", "whet2"])]
    return iterate(commands)


def whetstone_run():
    commands = [("Whetstone default:", [location + "/whet"]),
                ("Langer et al.:", [location + "/whet2"])]
    return iterate(commands)


def ramspeed_build():
    commands = [("", [location + "/ramspeed/build.sh"])]
    return iterate(commands, location + "/ramspeed")

ramsmp = {
    1: "INTmark write",
    2: "INTmark read",
    3: "INT memory",
    4: "FLOATmark write",
    5: "FLOATmark read",
    6: "FLOAT memory",
    7: "MMXmark write",
    8: "MMXmark read",
    9: "MMX memory",
    10: "SSEmark write",
    11: "SSEmark read",
    12: "SSE memory",
    13: "MMXmark (nt) write",
    14: "MMXmark (nt) read",
    15: "MMX (nt) memory",
    16: "SSEmark (nt) write",
    17: "SSEmark (nt) read",
    18: "SSE (nt) memory",
}


def ramspeed_run():
    # 32 GB per batch (use double the ram capacity), others default, run all tests
    # recomended 5 number of runs
    # Does it cause issue to use -l all the time?
    # Only use 2 processes as using 8 led to too long execution time (over an hour vs 20min)
    commands = []
    for i in range(1, 19):
        commands.append(("Ramspeed -b {}".format(ramsmp[i]),
                        ["./ramspeed/ramsmp", "-b", "{}".format(i), "-g", "32", "-l", "5"]))
    return iterate(commands)


def dbench_build():
    return "", 0


def dbench_run():
    # 180 seconds - reasonable ammount of operations is done
    # default settings
    # 1, 2, 4, 8 processes to see the effect
    # None, fsync, O_SYNC
    # If /dbench folder exists use it as root (docker volume)
    commands = []
    command = ["dbench", "-t", "180"]
    if os.path.isdir('/dbench'):
        dir = ["-D", "/dbench"]
        for i in (1, 2, 4, 8):
            commands.append(("Dbench proc: {}".format(i),
                            command + dir + ["{}".format(i)]))
        for i in (1, 2, 4, 8):
            commands.append(("Dbench fsync proc: {}".format(i),
                            command + dir + ["-F", "{}".format(i)]))
        for i in (1, 2, 4, 8):
            commands.append(("Dbench O_SYNC proc: {}".format(i),
                             command + dir + ["-s", "{}".format(i)]))
    for i in (1, 2, 4, 8):
        commands.append(("Dbench proc: {}".format(i),
                        command + ["{}".format(i)]))
    for i in (1, 2, 4, 8):
        commands.append(("Dbench fsync proc: {}".format(i),
                        command + ["-F", "{}".format(i)]))
    for i in (1, 2, 4, 8):
        commands.append(("Dbench O_SYNC proc: {}".format(i),
                        command + ["-s", "{}".format(i)]))
    output1, ret1 = disk_benchmark_prepare()
    output2, ret2 = iterate(commands)
    return output1 + "\n" + output2, ret1 + ret2


def kcbench_build():
    commands = [("", [location + "/kcbench/kcbench", "-b"])]
    return iterate(commands)


def kcbench_run():
    commands = [("", [location + "/kcbench/kcbench", "-r"])]  # TODO
    return iterate(commands)


def iozone_build():
    commands = [("", ["make", "linux-AMD64"])]
    return iterate(commands, location + "/iozone/src/current")


def iozone_run():
    # -g memory ammount is supposed to be double of ram (documentation)
    # -a - full automatic mode to try different sizes + -z to test even small sizes
    # -R excel output, -e include fsync and fflush in the timing
    cmd = [location + "/iozone/src/current/iozone", "-aRe", "-g", "2G"]
    for i in range(13):
        cmd.append("-i")
        cmd.append("{}".format(i))
    commands = [("IOzone small", cmd)]
    cmd = [location + "/iozone/src/current/iozone", "-Re", "-g", "32G"]
    for i in range(13):
        cmd.append("-i")
        cmd.append("{}".format(i))
    commands.append(("IOzone large", cmd))

    output1, ret1 = disk_benchmark_prepare()
    output2, ret2 = iterate(commands)
    return output1 + "\n" + output2, ret1 + ret2



def bonnie_build():
    return "", 0


def bonnie_run():
    # RAM size - RAM available
    # -x 3 - run 3x, -n 2000 samples for enough to calculate all metrics
    # -q quiet
    # Requires non-root
    commands = [("Bonnie++", ["bonnie++", "-q", "-r", "16000", "-z", "158230295", "-x", "3", "-n", "2500", "-u", "1000"])]
    output1, ret1 = disk_benchmark_prepare()
    output2, ret2 = iterate(commands)
    return output1 + "\n" + output2, ret1 + ret2


def iperf_build():
    return "", 0


def iperf_run(ip):
    # -O omit first 10 seconds, -t 180 run for 3 minutes
    # do both tcp and udp, unlimited bandwidth, -b maximum interface bandwidth in bits per second
    # Potentionally add parallel
    commands = [("UDP", ["iperf3", "-c", ip, "-O", "10", "-i", "60", "-t", "180", "-u", "-b",
                         "0", "--cport", "53333", "-b", "1000000000"]),
                ("UDP unlimited", ["iperf3", "-c", ip, "-O", "10", "-i", "60", "-t", "180", "-u",
                                   "-b", "0", "--cport", "53333"]),
                ("TCP", ["iperf3", "-c", ip, "-O", "10",  "-i", "60", "-t", "180",
                         "--cport", "53333", "-b", "1000000000"]),
                ("TCP unlimited", ["iperf3", "-c", ip, "-O", "10", "-i", "60", "-t", "180",
                                   "-b", "0", "--cport", "53333"])]
    return iterate(commands)
