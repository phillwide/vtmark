#!/usr/bin/python3
from datetime import datetime
from datetime import timedelta
from handlers import *
import argparse
from flask import Flask

app = Flask(__name__)


class Executor:
    "Benchmark abstraction class"
    def __init__(self, description, run, build):
        self.description = description
        self.run = run
        self.build = build


executor = {
    0: Executor("Dhrystone benchmark", dhrystone_run, dhrystone_build),
    1: Executor("Whetstone benchmark", whetstone_run, whetstone_build),
    2: Executor("Ramspeed benchmark", ramspeed_run, ramspeed_build),
    3: Executor("Dbench benchmark", dbench_run, dbench_build),
    4: Executor("Kernel kompilation benchmark", kcbench_run, kcbench_build),
    5: Executor("IOZone benchmark", iozone_run, iozone_build),
    6: Executor("Bonnie++ benchmark", bonnie_run, bonnie_build),
    7: Executor("iPerf3 benchmark (ip addr)", iperf_run, iperf_build),
}


def help_string():
    description = "Choose to run one of the following benchmarks:\n"
    for i in range(len(executor)):
        description += "\t {}: {}\n".format(i, executor[i].description)
    description += "Use this url: /benchmark/<number>[/<ip_addr>]\n"
    return description


@app.route("/")
def help():
    return help_string(), 200

@app.route("/benchmark/<int:choice>/<ip>")
def benchmark_iperf(choice=7, ip=None):
    return benchmark(choice, ip)

@app.route("/benchmark/<int:choice>")
def benchmark(choice=0, ip="10.20.105.13"):
    if choice < 0 or choice >= len(executor):
        return "Error: invalid benchmark\n" + help_string(), 1

    output, ret = executor[choice].build()
    if ret != 0:
        return "Build finished with error: {}\n{}".format(ret, output), ret
    start = datetime.now()
    if choice == 7 or choice == 8:
        output, ret = executor[choice].run(ip)
    else:
        output, ret = executor[choice].run()
    end = datetime.now()
    output += "Date:: Start: {} End: {}\n".format(start.strftime("%Y-%m-%d %H:%M:%S"),
                                                  end.strftime("%Y-%m-%d %H:%M:%S"))
    output += "Timestamp:: Start: {} End: {}\n".format(datetime.timestamp(start),
                                                       datetime.timestamp(end))
    output += "Duration:: {}\n".format(timedelta(seconds=datetime.timestamp(end) - datetime.timestamp(start)))
    if ret != 0:
        output += "Finished with error: {}\n".format(ret)
    if ret != 0:
        return output, 500
    else:
        return output, 200


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", help="Select port to run on", type=int, default=80)
    args = parser.parse_args()
    return args.port


def main():
    app.run(host='0.0.0.0', port=parse(), threaded=True)


if __name__ == "__main__":
    main()
