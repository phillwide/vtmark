# KVM Setup

Create VM: virt-install --virt-type kvm --name kvm-vm2 --vcpus 8 --cpu host --location http://deb.debian.org/debian/dists/buster/main/installer-amd64/ --os-variant debian10 --disk size=60,sparse=false --memory 14336 --graphics none --console pty,target_type=serial --extra-args "console=ttyS0" --network bridge=br0 --hvm

The guides used:

https://wiki.debian.org/KVM
