# Xen Project Setup

The setup of Xen project has not been done using ansible.
This file contains the commands used to set it up.

The guides used:

https://wiki.xenproject.org/wiki/Xen_Project_Beginners_Guide
https://wiki.debian.org/Xen

## Install Xen Project

apt-get install xen-system xen-tools

## Fix Grub and Reboot
```
dpkg-divert --divert /etc/grub.d/08_linux_xen --rename /etc/grub.d/20_linux_xen
update-grub
reboot
```

## Configure VM settings

In /etc/xen/xen-vm.cfg:
```
size   = 60G
memory = 14153M
swap   = 512M     # Suffix (G, M, k) required
fs     = ext4     # Default file system for any disk
dist   = `xt-guess-suite-and-mirror --suite`
image  = sparse

gateway    = 10.20.0.1
netmask    = 255.255.0.0
nameserver = 8.8.8.8
```

## Create VM

```
xen-create-image --hostname xen-vm --ip 10.20.105.240 --vcpus 8 --pygrub --dist buster
modprobe loop max_loop=255
xl create /etc/xen/xen-vm.cfg
```

The following bridging was setup in /etc/network/interfaces:
```
auto lo
iface lo inet loopback

iface enp2s0f0 inet manual

auto xenbr0
iface xenbr0 inet static
address 10.20.105.24
netmask 255.255.0.0
bridge_ports enp2s0f0
bridge_fd 0
bridge_waitport 0
```
