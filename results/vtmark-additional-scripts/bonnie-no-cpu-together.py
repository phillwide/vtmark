#!/usr/bin/python3
import argparse

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Select file to process")
    return parser.parse_args()

def main():
    args = parse()
    file = open(args.file)
    new_lines = {}
    average = {}
    tech_number = 12
    modulo = tech_number + 2
    count = 0
    output = {}
    for line in file:
        if count % modulo > 1:
            data = line.split(",")
            #print(data[0], end=", ")
            for i in range(1, len(data), 2):
                if count % modulo not in output:
                    output[count % modulo] = []
                output[count % modulo].append(data[i])
        count += 1

    count = 0
    file.seek(0)
    for line in file:
        if count % modulo > 1:
            print(line.split(",")[0], end=", ")
            print(", ".join(output[count % modulo]))
        count += 1
        if count == modulo:
            break

if __name__ == "__main__":
    main()
