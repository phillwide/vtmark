#!/usr/bin/python3
import argparse

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Select file to process")
    return parser.parse_args()

def main():
    args = parse()
    file = open(args.file)
    new_lines = {}
    average = {}
    tech_number = 12
    modulo = tech_number + 2
    count = 0
    for line in file:
        if count % modulo > 1:
            data = line.split(",")
            print(data[0], end=", ")
            for i in range(1, len(data), 2):
                print(data[i], end=", ")
            print()
        elif count % modulo == 0:
            print(line, end="")
        count += 1

if __name__ == "__main__":
    main()
