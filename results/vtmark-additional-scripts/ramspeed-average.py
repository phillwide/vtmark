#!/usr/bin/python3
import argparse

def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Select file to process")
    return parser.parse_args()

def main():
    args = parse()
    file = open(args.file)
    new_lines = {}
    average = {}
    tech_number = 8
    modulo = tech_number + 1
    count = 0
    for line in file:
        if count % modulo not in new_lines:
            data = line.rstrip().split(",")
            if count % modulo:
                new_lines[count % modulo] = data[0] + ", " + str(float(data[-1]))
                average[count % modulo] = float(data[-1])
            else:
                new_lines[count % modulo] = data[0]

        else:
            new_lines[count % modulo] += ", " + str(float(line.rstrip().split(",")[-1])) if count % modulo else  ", " + line.rstrip().split(",")[-1]
            if  count % modulo:
                average[count % modulo] += float(line.rstrip().split(",")[-1])
        count += 1

    for item in new_lines:
        if  item:
            print(new_lines[item] + ", " + str(average[item]/len(new_lines[0].split(","))))
        else:
            print(new_lines[item] + ", Average")

if __name__ == "__main__":
    main()
