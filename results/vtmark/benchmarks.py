#!/usr/bin/python3
""" Functions for processing benchmark result into common format
"""
import argparse
from argparse import RawDescriptionHelpFormatter
import itertools
import os
import re
import statistics
import pprint
from vtmark.technologies import *
from vtmark.file_processing import *

def common_handler(bench, start, stop):
    tech = dict()
    for technology in technologies:
        tech[technology[0]] = dict()
        results = dict()
        for i in range(start, stop):
            if bench == 1:
                benchmarks, data = dhrystone_process_file("dhrystone/" + technology[1] + "-" + str(i))
            elif bench == 2:
                benchmarks, data = whetstone_process_file("whetstone/" + technology[1] + "-" + str(i))
            for benchmark, value in zip(benchmarks, data):
                if benchmark not in results:
                    results[benchmark] = []
                results[benchmark].append(float(value))
        tech[technology[0]]["raw"] = results
    return tech

def dhrystone(table=False):
    return common_handler(1, 1, 6)

def whetstone(table=False):
    return common_handler(2, 1, 6)

def ramspeed(table=False):
    ramspeed_types = ["INTEGER", "FL-POINT", "MMX", "SSE", "MMX \(nt\)", "SSE \(nt\)"]
    output = dict()
    for type in ramspeed_types:
        output[type] = []
        for technology in technologies:
            output[type].append(ramspeed_process_one(type, technology[0], technology[1]))
    return output

def dbench(table=False):
    tech = dict()
    output = dict()
    benchs = ["dbench", "dbench fsync", "dbench O_SYNC"]
    for item in benchs:
        output[item] = []
    for technology in technologies_dbench:
        tech[technology[0]] = dict()
        benchmarks, results = dbench_process_file("dbench/" + technology[1])
        for bindex in range(0, 3):
            row = []
            row.append(technology[0])
            for mindex in range (0, 4):
                tput = float(re.findall('Throughput (.*) MB',results[bindex*4 + mindex])[0])
                mlat = float(re.findall('max_latency=(.*) ms',results[bindex*4 + mindex])[0])
                row.append(tput)
                if table:
                    row.append(mlat)
            output[benchs[bindex]].append(row)
    return output


def kcbench(table=False):
    tech = dict()
    for technology in technologies:
        tech[technology[0]] = dict()
        tech[technology[0]]["raw"] = dict()
        input = open("kernel/" + technology[1])
        content = input.read()
        points = []
        time = []
        cpu = []
        benchmarks = re.findall('^Run [123] \(\-j [1-9]+\):      (.*) points  \(e\:(.*) sec  P\:(.*)\%  U\:[0-9.]+ sec  S\:[0-9.]+ sec\)', content, re.MULTILINE)
        for benchmark in benchmarks:
            points.append(int(benchmark[0]))
            time.append(float(benchmark[1]))
            cpu.append(int(benchmark[2]))

        tech[technology[0]]["raw"]["POINTS"] = points
        if table:
            tech[technology[0]]["raw"]["TIME"] = time
            tech[technology[0]]["raw"]["CPU"] = cpu
    return tech

def bonnie(table=False):
    all_results = dict()
    for technology in technologies_bonnie:
        input = open("bonnie/" + technology[1])
        content = input.read()
        all_results[technology[0]]  = dict()
        tests = re.findall('^format_version,bonnie_version,name,(.*)', content, re.MULTILINE)[0].split(",")
        benchmarks = re.findall('^1.98,1.98,(.*)', content, re.MULTILINE)
        for benchmark in benchmarks:
            data = benchmark.split(',')
            for index in range(1,len(data)):
                match = re.search('^([0-9]*)([a-zA-Z]*)', data[index])
                if index not in all_results[technology[0]]:
                    all_results[technology[0]][index] = []
                if match.group(1) != '':
                    all_results[technology[0]][index].append(int(match.group(1)))
        input.close()

    groups = {
    "Sequential Output": [7, 13],
    "Sequential Input": [13, 17],
    "Sequential Create": [24, 30], # 23, 30
    #"Random Create": [60, 36]
    }

    output = dict()
    for group in groups:
        output[group] = dict()
        for item in all_results:
            output[group][item] = {"raw": dict()}
            for index in range(groups[group][0], groups[group][1]):
                output[group][item]["raw"][index] = all_results[item][index]

    return output

def iperf(table=False):
    results = {
    "UDP": [],
    "TCP": []
    }

    for technology in technologies:
        results["UDP"].append(iperf_process_file(technology, 0, 4, 1))
    for technology in technologies:
        results["TCP"].append(iperf_process_file(technology, 4, 8, 2))

    return results
