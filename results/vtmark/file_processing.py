#!/usr/bin/python3
""" Parsing result data from files for processing in vtmark.benchmarks
"""
import argparse
from argparse import RawDescriptionHelpFormatter
import itertools
import os
import re
import statistics
import pprint
from vtmark.technologies import *

def dhrystone_process_file(filename):
    input = open(filename)
    content = input.read()
    benchmarks = re.findall('^Dhrystone (.*): Output:$', content, re.MULTILINE)
    dhrystones = re.findall('Second: (.*)$', content, re.MULTILINE)
    return benchmarks, dhrystones

def whetstone_process_file(filename):
    input = open(filename)
    content = input.read()
    benchmarks = re.findall('^(.*): Output:$', content, re.MULTILINE)
    results = re.findall('^MWIPS\s+([0-9.]*)\s+([0-9.]*)', content, re.MULTILINE)
    new_results = []
    for result in results:
        new_results.append(result[0])
    return benchmarks, new_results

def ramspeed_process_one(type, name, file):
    input = open("ramspeed/" + file)
    content = input.read()
    results = re.findall('^{} BatchRun (.*) (.*) MB/s'.format(type), content, re.MULTILINE)
    row = []
    row.append(name)
    for item in results:
        row.append(item[1])
    input.close()
    return row

def dbench_process_file(filename):
    input = open(filename)
    content = input.read()
    benchmarks = re.findall('^Dbench .*$', content, re.MULTILINE)
    results = re.findall('^Throughput .*$', content, re.MULTILINE)
    return benchmarks, results


def iperf_process_file(technology, start, end, step):
    input = open("iperf3/" + technology[1])
    output = []
    content = input.read()
    results = re.findall('^(.*) (sender|receiver)', content, re.MULTILINE)
    output.append("{} ".format(technology[0]))
    for index in range(start, end, step):
        rmatch = re.search('GBytes\s+([0-9]*(\.[0-9]+)?) (M|G)bits/sec', results[index][0])
        if rmatch.group(0).split()[2] == "Gbits/sec":
            data = int(float(rmatch.group(0).split()[1]) * 1000)
        else:
            data = rmatch.group(0).split()[1]
        output.append("{}".format(data))
    return output
