import statistics
from vtmark.benchmarks import *
import pprint

# Average
def average(list):
    return sum(list) / len(list)

# Relative standard deviation
def rstd(list):
    return statistics.stdev(list)*100/average(list)

class Processor:
    "Benchmark abstraction class"
    def __init__(self, description, run, func=0, value_type=int, treshold=5, diviation_skip=[]):
        self.description = description
        self.run = run
        self.type = value_type
        self.treshold = treshold
        self.func = func
        self.diviation_skip = diviation_skip
        self.rstd = False

    def process(self, table=True, relative=False):
        if self.func == 0:
             self.run(table)
        elif self.func == 1:
            self.average_process(table, relative)
        elif self.func == 2:
            self.process_multiple(table, relative)
        elif self.func == 3:
            self.average_process_multiple(table, relative)

    def average_process(self, table=True, relative=False):
        """ Process benchmark data for average (benchmarks that ran multiple times)
            Parse files and get data, process the data, and print results
        """
        raw_data = self.run(table)
        self._average_process(raw_data, table, relative)

    def average_process_multiple(self, table=True, relative=False):
        """ Process benchmark data for average (benchmarks that ran multiple times)
            Parse files and get data, process the data, and print results
            Special case for Bonnie++ to split the results into multiple sections
        """
        raw_data = self.run(table)
        for benchmark in raw_data:
            self._average_process(raw_data[benchmark], table, relative)

    def _average_process(self, dictionary, table=True, relative=False):
        """ Process benchmark data
            Parse files and get data, process the data, and print results
        """
        include_minmax = []
        # Process each technology
        for tech in dictionary:
            processed, diviation = self.get_data(dictionary[tech]["raw"])
            dictionary[tech]["proc"] = processed
            include_minmax += diviation

        if relative:
            first = list(dictionary.keys())[0]
        else:
            first = None
        if self.rstd:
            self.print_rstd(dictionary)
        else:
            self.print_results(dictionary, include_minmax, table, first)

    def process_multiple(self, table=True, relative=False):
        """ Processes and prints the results for benchmarks that ran once (over period of time)
        """
        # Benchmark ran once, rstd does not apply
        if self.rstd > 0:
            return
        results = self.run(table)
        for benchmark in results:
            # When calculating relative results, the first defined technology is used
            if relative:
                first = results[benchmark][0]
            else:
                first = None
            for row in results[benchmark]:
                if table:
                    print('\\bfseries ', end="")
                print(row[0], end="")
                for i in range(1, len(row)):
                    if table:
                        print(" & ", end="")
                    else:
                        print(", ", end="")
                    value = float(row[i])
                    if first:
                        value = value / float(first[i])*100
                    print("%.2f" % value, end="")
                if table:
                    print(" \\\ \hline")
                else:
                    print()


    def get_data(self, dictionary):
        """ Processes results of a technology
            Input: Dictionary of different subbenchmarks
                The value is a list of results from multiple runs of the benchmark
                An average, min, max, and relative standard deviation is calculated
        """
        diviation = []
        results = dict()
        for item in dictionary:
            results[item] = dict()
            if len(dictionary[item]) > 1:
                item_rstd = rstd(dictionary[item])
                if item_rstd >= self.treshold:
                    diviation.append(item)
                results[item]["rstd"] = item_rstd
                results[item]["avg"] = average(dictionary[item])
                results[item]["min"] = min(dictionary[item])
                results[item]["max"] = max(dictionary[item])
            elif len(dictionary[item]) == 1:
                results[item]["rstd"] = 0
                results[item]["avg"] = dictionary[item][0]
                results[item]["min"] = dictionary[item][0]
                results[item]["max"] = dictionary[item][0]
            else:
                results[item]["rstd"] = 0
                results[item]["min"] = 0
                results[item]["max"] = 0
        return results, diviation

    def print_results(self, dictionary, diviation, table, first):
        """ Prints the results from benchmarks ran multiple times
        """
        # Print column names (Add min and max columns for measurements with higher rstd)
        diviation = [x for x in diviation if x not in self.diviation_skip]
        tmp = list(dictionary.keys())[0]
        for item in dictionary[tmp]["proc"]:
            print("{} ".format(item), end="")
            if item in diviation:
                print("{}-min {}-max ".format(item, item), end="")
        print()
        # Print results
        ## Iterate over technologies
        for tech in dictionary:
            if table:
                print("\\bfseries {}".format(tech), end="")
            else:
                print("{}".format(tech), end="")
            bench_data = dictionary[tech]["proc"]
            for benchmark in bench_data:

                if table:
                    print(" & ", end="")
                else:
                    print(", ", end="")
                if "avg" not in bench_data[benchmark]:
                    value = "n/a"
                else:
                    value = self.type(round(bench_data[benchmark]["avg"]))
                if first and value != "n/a":
                    value = float(value)/float(dictionary[first]["proc"][benchmark]["avg"])*100
                print('{:,}'.format(value), end="")
                if benchmark in diviation:
                    if table:
                        print(' & {:,} & {:,}'.format(self.type(round(bench_data[benchmark]["min"])),
                                                 self.type(round(bench_data[benchmark]["max"]))), end="")
                    else:
                        print(' , {} , {}'.format(self.type(round(bench_data[benchmark]["min"])),
                                                 self.type(round(bench_data[benchmark]["max"]))), end="")
            if table:
                print(" \\\ \hline")
            else:
                print()

    def print_rstd(self, dictionary):
        """ Prints the rstd of each technology and subbenchmark
        """
        # Print subbenchmarks
        if self.rstd == 1:
            for subbenchmark in dictionary[list(dictionary.keys())[0]]["proc"]:
                print("{} ,".format(subbenchmark), end="")
            print()
        # Print rstd per technology and subbenchmark
        if self.rstd != 3:
            for tech in dictionary:
                print("{} ".format(tech), end="")
                bench_data = dictionary[tech]["proc"]
                if self.rstd == 1:
                    for benchmark in bench_data:
                        if "avg" not in bench_data[benchmark]:
                            value = "n/a"
                        else:
                            value = bench_data[benchmark]["rstd"]
                        print(", {}".format(value), end="")
                    print()
                elif self.rstd == 2:
                    value = 0
                    for benchmark in bench_data:
                        if benchmark in self.diviation_skip:
                            continue
                        if "avg" in bench_data[benchmark]:
                            value = max(bench_data[benchmark]["rstd"], value)
                    print(", {}".format(value))
        else:
            value = 0
            for tech in dictionary:
                bench_data = dictionary[tech]["proc"]
                for benchmark in bench_data:
                        if benchmark in self.diviation_skip:
                            continue
                        if "avg" in bench_data[benchmark]:
                            value = max(bench_data[benchmark]["rstd"], value)
            print(", {}".format(value), end="")
