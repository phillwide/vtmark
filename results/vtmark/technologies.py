technologies = [
("Bare-metal", "baremetal"),
("OpenStack Bare-metal", "openstack-baremetal"),
("Docker", "openstack-docker"),
("Podman", "openstack-podman"),
("VirtualBox", "openstack-virtualbox"),
("VirtualBox Recommended", "openstack-virtualbox-recommended"),
("VMWare", "openstack-vmware"),
("Xen Project", "xen"),
("Xen Project 2", "xen-vm"),
("KVM", "kvm"),
]

technologies_dbench = [
("Bare-metal", "baremetal"),
("OpenStack Bare-metal", "openstack-baremetal"),
("Docker", "openstack-docker"),
("Docker Volume", "openstack-docker-volume"),
("Docker Bare-metal", "docker"),
("Docker Bare-metal Volume", "docker-volume"),
("Podman", "openstack-podman"),
("Podman Volume", "openstack-podman-volume"),
("Podman Bare-metal", "podman"),
("Podman Bare-metal Volume", "podman-volume"),
("VirtualBox HDD", "openstack-virtualbox"),
("VirtualBox SSD", "openstack-virtualbox-hdd"),
("VirtualBox Rec.", "openstack-virtualbox-recommended"),
("VirtualBox SCSI.", "openstack-virtualbox-scsi"),
("VMWare", "openstack-vmware"),
("Xen Project", "xen"),
("Xen Project 2", "xen-vm"),
("KVM", "kvm")
]

technologies_bonnie = [
("Bare-metal", "baremetal"),
("OpenStack Bare-metal", "openstack-baremetal"),
("Docker", "openstack-docker"),
("Docker Bare-metal", "docker"),
("Podman", "openstack-podman"),
("Podman Bare-metal", "podman"),
("VirtualBox HDD", "openstack-virtualbox"),
("VirtualBox SSD", "openstack-virtualbox-hdd"),
("VirtualBox Rec.", "openstack-virtualbox-recommended"),
("VirtualBox SCSI.", "openstack-virtualbox-scsi"),
("VMWare", "openstack-vmware"),
("Xen Project", "xen"),
("Xen Project 2", "xen-vm"),
("KVM", "kvm")
]
