#!/usr/bin/python3
import argparse
from argparse import RawDescriptionHelpFormatter
import requests
import os
import time

benchmarks = {
    0: ("Dhrystone benchmark", "dhrystone", 1),
    1: ("Whetstone benchmark", "whetstone", 1) ,
    2: ("Ramspeed benchmark", "ramspeed", 0),
    3: ("Dbench benchmark", "dbench", 0),
    4: ("Kernel compilation benchmark", "kernel", 0),
    5: ("IOZone", "iozone", 0),
    6: ("Bonnie++ benchmark", "bonnie", 0),
    7: ("iPerf3 benchmark (ip addr)", "iperf3", 2),
}

cwd = os.path.dirname(os.path.realpath(__file__)) + "/"

def help_string():
    description = "VTMark executor\n \
    Run and save benchmark results.\n\
    You can run the following benchmarks:\n"
    for i in range(len(benchmarks)):
        description += "\t {}: {}\n".format(i, benchmarks[i][0])
    return description

def parse():
    parser = argparse.ArgumentParser(description=help_string(), formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("-s", "--skip", nargs='+', type=int, help="Specify benchmarks to skip")
    parser.add_argument("-p", "--port", type=int, default=4000, help="Specify host port")
    parser.add_argument("-t", "--target", default="10.20.105.13", help="Specify target server for benchmark")
    parser.add_argument("host", help="Specify host to benchmark")
    parser.add_argument("name", help="Specify name of the host technology")

    return parser.parse_args()

def execute(host, port, benchmark, param=""):
    print("http://{}:{}/benchmark/{}/{}".format(host, port, benchmark, param))
    response = requests.get("http://{}:{}/benchmark/{}{}{}".format(host, port, benchmark, "/" if param != "" else "", param))
    return response.text, response.status_code

def save(filename, data):
    file = open(filename, "w")
    file.write(data)
    file.close()

def run_once(host, name, port, benchmark):
    result, ret = execute(host, port, benchmark)
    filename = cwd + benchmarks[benchmark][1] + "/" + name
    save(filename, result)
    return ret

def run_multiple(host, name, port, benchmark):
    rets = 200
    for i in range(1, 6):
        result, ret = execute(host, port, benchmark)
        filename = cwd + benchmarks[benchmark][1] + "/" + name + "-" + str(i)
        save(filename, result)
        rets = ret if ret != 200 else rets
        time.sleep(60)
    return rets

def run_client_server(host, name, port, benchmark, target):
    result, ret = execute(host, port, benchmark, target)
    filename = cwd + benchmarks[benchmark][1] + "/" + name
    save(filename, result)
    return ret

def main():
    args = parse()
    wait = False
    for i in range(0, len(benchmarks)):
        dir = benchmarks[i][1]
        exec = benchmarks[i][2]
        # Skip if requested
        if args.skip is not None and i in args.skip:
            print(benchmarks[i][0], "skipped")
            continue
        # Sleep for 10 minutes in between benchmarks
        if wait:
            wait = False
            time.sleep(600)
        # Ensre dir exists
        if not os.path.exists(dir):
            os.makedirs(dir)
        # Choose execution function
        if exec == 0:
            run_once(args.host, args.name, args.port, i)
        elif exec == 1:
            run_multiple(args.host, args.name, args.port, i)
        elif exec == 2:
            run_client_server(args.host, args.name, args.port, i, args.target)
        wait = True



if __name__ == "__main__":
    main()
