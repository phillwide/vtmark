#!/usr/bin/python3
import argparse
from argparse import RawDescriptionHelpFormatter
from vtmark.benchmarks import *
from vtmark.processor import *

processors = {
    0: Processor("All", None, 0),
    1: Processor("Dhrystone benchmark", dhrystone, 1),
    2: Processor("Whetstone benchmark", whetstone, 1) ,
    3: Processor("Ramspeed benchmark", ramspeed, 2),
    4: Processor("Dbench benchmark", dbench, 2),
    5: Processor("Kernel compilation benchmark", kcbench, 1),
    6: Processor("Bonnie++ benchmark", bonnie, 3, int, 5, [10, 12, 14, 16, 25, 27, 29]),
    7: Processor("iPerf3 benchmark (ip addr)", iperf, 2),
}

def help_string():
    description = "Choose to run one of the following benchmarks:\n"
    for i in range(len(processors)):
        description += "\t {}: {}\n".format(i, processors[i].description)
    return description

def parse():
    parser = argparse.ArgumentParser(description=help_string(), formatter_class=RawDescriptionHelpFormatter)
    parser.add_argument("benchmark", type=int, help="Select benchmark to process")
    parser.add_argument("-x", "--excel", action='store_true', help="Output data in excel format")
    parser.add_argument("-r", "--relative", action='store_true', help="Output data relative to first item")
    parser.add_argument("-t", "--treshold", type=int, default=5, help="Change the default relative standard deviation from 5")
    parser.add_argument("-s", "--rstd", type=int, default=0, help="Output relative standard deviation\n1: Show rstd per subbenchmark\n2: Show maximal rstd per technology\n3: Show maximal rstd per benchmark")
    return parser.parse_args()

def run_all(args):
    """ Runs all benchmark result parsing
    """
    benchmarks = []
    # Print benchmarks as columns when needed for rstd 3
    if args.rstd == 3:
        print("Benchmark, ", end="")
        for bench in range(1, len(processors)):
            if  processors[bench].func != 2:
                benchmarks.append(processors[bench].description)
        print(", ".join(benchmarks))
        print("RSTD", end="")
    for bench in range(1, len(processors)):
        # Skip those benchmarks that were not run multiple times
        if args.rstd != 0 and processors[bench].func == 2:
            continue
        # Print separation if we dont need the benchmarks as columns
        if args.rstd != 3:
            print()
            print(processors[bench].description)
        processors[bench].treshold = int(args.treshold)
        processors[bench].rstd = args.rstd
        processors[bench].process(not args.excel, args.relative)
    print()

def main():
    """ Parse results and output required information.
        Specify technologies to parse in vtmark.technologies,
        The format is: 'Name for printing': 'Name of a folder with results'
    """
    args = parse()
    if args.benchmark == 0:
        run_all(args)
    else:
        processors[args.benchmark].treshold = int(args.treshold)
        processors[args.benchmark].rstd = args.rstd
        processors[args.benchmark].process(not args.excel, args.relative)


if __name__ == "__main__":
    main()
