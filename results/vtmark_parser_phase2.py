#!/usr/bin/python3
import argparse
import re

technologies = [
("Bare-metal", "baremetal"),
("OpenStack Bare-metal", "openstack-baremetal"),
("Docker", "openstack-docker"),
("Podman", "openstack-podman"),
("VirtualBox", "openstack-virtualbox"),
("VirtualBox Recommended", "openstack-virtualbox-recommended"),
("VMWare", "openstack-vmware"),
]

levels = [500, 1000, 1400, 1750, 2000, 4000, 8000, 12000]

http_codes = ["200", "500", "502", "503", "504"]

other_error_codes = ["Couldn't connect to server",
               "Transferred a partial file", "Timeout was reached",
               "SSL connect error", "Failed binding local connection end",
               "Server returned nothing (no headers, no data)",
               "Failed sending data to the peer",
               "Failure when receiving data from the peer"
               ]

codes = http_codes + other_error_codes

def parse():
    parser = argparse.ArgumentParser(description="Parse phase 2 results (Case study)")
    parser.add_argument("-s", "--success", action='store_true', help="Output only success")
    parser.add_argument("--success-percent", action='store_true', help="Output percent of successful searches")
    parser.add_argument("-t", "--table", action='store_true', help="Output in latex table format")
    parser.add_argument("-e", "--errors", action='store_true', help="Show errors")
    parser.add_argument("-w", "--web", action='store_true', help="Show http codes")
    parser.add_argument("-c", "--count", action='store_true', help="Count errors")
    parser.add_argument("-r", "--relative", action='store_true', help="Output the results relative to baremetal")
    return parser.parse_args()

def process():
    results = dict()
    for technology in technologies:
        results[technology] = dict()
        for level in levels:
            results[technology][level] = dict()
            file = open("phase2/" + technology[1] + "/" + technology[1] + "-" + str(level))
            content = file.read()
            file.close()
            # process
            for code in codes:
                regex = "{}: ([0-9]+)".format(code)
                rmatch = re.findall(regex, content)
                if rmatch:
                    results[technology][level][code] = int(rmatch[0])
                else:
                    results[technology][level][code] = 0
    return results

def printer(results, keys, table=True, count=False):
    if table:
        separator = " & "
    else:
        separator = ", "
    print("Technology & Users", end=separator)
    print(separator.join(keys))
    for technology in technologies:
        for level in levels:
            if table:
                if level == levels[0]:
                    print('\\multirow{7}{*}{' + technology[0] + '}', end=separator)
                else:
                    print("\t", end=separator)
            else:
                print(technology[0], end=separator)
            print(level*4, end=separator)
            for code in codes:
                if code in keys:
                    print(results[technology][level][code], end="")
                    if code != keys[-1]:
                        print(end=separator)
            if table:
                if level == levels[-1]:
                    print(' \\\\ \\hline')
                else:
                    print(' \\\\')
            else:
                print()

def printer_success(results, table, relative, percent=False):
    if table:
        separator = " & "
    else:
        separator = ", "
    print("Technology", end=separator)
    for level in levels:
        print(level*4,end="")
        if level != levels[-1]:
            print(end=separator)
    print()
    for technology in technologies:
        print(technology[0], end=separator)
        for level in levels:
            value = results[technology][level]["200"]
            errors = 0
            for code in codes:
                if code == "200":
                    continue
                errors += results[technology][level][code]
            if percent and errors + value > 0:
                value = 100/(errors + value)*value
            elif relative:
                value /= results[technologies[0]][level]["200"]
                value *= 100
            print(value, end="")
            if level != levels[-1]:
                print(end=separator)
        if table:
            print(" \\\\ ")
        else:
            print()
11
def printer_errors(results, table, relative):
    print()

def main():
    args = parse()
    results = process()
    if args.count:
        printer_success(results, args.table, args.relative)
    elif args.success or args.success_percent:
        printer_success(results, args.table, args.relative, args.success_percent)
    elif args.errors:
        printer(results, other_error_codes, args.table)
    elif args.web:
        printer(results, http_codes, args.table)
    else:
        printer(results, codes, args.table)



if __name__ == "__main__":
    main()
